import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.css']
})
export class CardDetailsComponent implements OnInit {
  @Input() dia: string = "";
  @Input() dia_mes: string = "";
  @Input() hora: string = "";
  @Input() humedad: string = "";
  @Input() temperatura: string = "";
  @Input() viento: string = "";


  constructor() { }

  ngOnInit(): void {
  }

}
