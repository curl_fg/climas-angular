import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-weather',
  templateUrl: './card-weather.component.html',
  styleUrls: ['./card-weather.component.css']
})
export class CardWeatherComponent implements OnInit {
  @Input() nombre: string = "";
  @Input() humedad: string = "";
  @Input() temperatura: string = "";
  @Input() viento: string = "";

  constructor() { }

  ngOnInit(): void {
  }

}
