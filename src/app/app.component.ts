import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cities = ["Buenos Aires", "Bogota", "Ciudad de Mexico", "Madrid", "Lima"];
  dataCities: any = [];
  dataForecast: any = [];
  currentCity: string = "";

  constructor(private weatherService: WeatherService) {
    this.cities.forEach(city => {
      this.weatherService.getWeather(city).subscribe(
        (res: any) => {
          this.dataCities.push({
            nombre: city,
            humedad: res.main.humidity,
            temperatura: res.main.temp,
            viento: res.wind.speed
          })
        },
        (err: HttpErrorResponse) => console.log(err.message)
      );
    });
  }

  private _getWeekDayStr(d: number) {
    switch (d) {
      case 0:
        return "Lunes";
      case 1:
        return "Martes";
      case 2:
        return "Miércoles";
      case 3:
        return "Jueves";
      case 4:
        return "Viernes";
      case 5:
        return "Sábado";
      case 6:
        return "Domingo";
    }

    return "";
  }
  // NOTE: creada para agrupar la key «dia» de «dataForecast», no se uso al final...
  private getGroupsDays() {
    let result = this.dataForecast.reduce((acumulado: any, iter: any) => {
      acumulado[iter.dia] = acumulado[iter.dia] || [];
      acumulado[iter.dia].push(iter);
      return acumulado;
    }, Object.create(null));

    return result;
  }

  getForecast(cityName: string) {
    this.currentCity = cityName;
    this.weatherService.getForecast(cityName).subscribe(
      (res: any) => {
        this.dataForecast = []; // Resetea los datos anteriormente guardados
        let forecastFiltered = res.list.filter((itm: any) => /(06|12|18):00:00$/.test(itm.dt_txt));

        forecastFiltered.forEach((itm: any) => {
          let d = new Date(itm.dt * 1000), weekDay = "";

          weekDay = this._getWeekDayStr(d.getDay());

          this.dataForecast.push({
            dia: weekDay,
            dia_mes: d.getDate(),
            hora: itm.dt_txt.split(" ")[1],
            humedad: itm.main.humidity,
            temperatura: itm.main.temp,
            viento: itm.wind.speed
          });
        });
      },
      (err: HttpErrorResponse) => console.log(err.message)
    );
  }
}