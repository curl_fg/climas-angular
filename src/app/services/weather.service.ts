import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  TOKEN: string = "";
  WEATHER: string = "";
  FORECAST: string = "";

  constructor(private httpClient: HttpClient) {
    let baseUrl = "https://api.openweathermap.org/data/2.5";
    // TODO: pasar el token como variable de entorno y no de manera literal
    this.TOKEN = "04551de766b947d01782447ee5e75145";
    this.WEATHER = `${baseUrl}/weather?q=`;
    this.FORECAST = `${baseUrl}/forecast?q=`;
  }

  getWeather(cityName: string) {
    return this.httpClient.get(`${this.WEATHER}${cityName}&appid=${this.TOKEN}&units=metric`);
  }

  getForecast(cityName: string) {
    return this.httpClient.get(`${this.FORECAST}${cityName}&appid=${this.TOKEN}&units=metric`);
  }
}